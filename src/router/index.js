import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/HomeView.vue";
// import MeView from "@/views/Me.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "",
    name: "toolbar",
    component: () => import("@/views/Toolbar.vue"),
    children: [
      {
        path: "/me",
        name: "me",
        component: () => import("@/views/Me.vue"),
      },
      {
        path: "/profile",
        name: "profile",
        component: () => import("@/views/Profile.vue"),
      },
    ],
  },
  {
    path: "/about",
    name: "about",
    component: () => import("../views/AboutView.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
